#pragma once

#include "cocos2d.h"
#include <iostream>
#include <fstream>
#include <ui\UITextField.h>

using namespace cocos2d;
class Level4 : public cocos2d::Scene
{
public:
	CREATE_FUNC(Level4);
	static cocos2d::Scene* createScene();
	virtual void onExit();
	virtual void onEnter();
	virtual bool init();
	void update(float deltaTime);
	void updateInputs();
	void updateMouseInputs();
	void updateKeyboardInputs();
	void UpdatePositions(float dt);
	void UpdateRot(float dt);
	void CheckCollision();
	void makePlat(int width, int height, int xPos, int yPos);
	void makeBack(int width, int height, int xPos, int yPos);
	bool jumpCheck();
	bool walkCheck();

	void menuCloseCallback(cocos2d::Ref* pSender);
	void DrawWorld();

	void Play(Ref *pSender);

	Vec2 chainVec;
	float maxChainLength = 350;

	bool hasPassedChainLength = false;
	bool hasObjectChild = false;

	float moveSpeed;
	float jumpHeight = 2850000;

private:

	Director* director;

	//Character
	Sprite* character;
	Sprite* ball;

	//Objects
	Sprite* power;
	Sprite* buttonBase1;
	Sprite* button1;
	Sprite* door;
	Sprite* g_exit;
	Sprite* movPlat;
	Sprite* crate;
	Sprite* crate2;
	Sprite* jumpSprite;
	Sprite* ledgeSprite;

	//Background
	Sprite* background;
	Sprite* background2;
	Sprite* platPath;
	Sprite* platformTemp;


	bool chained = true; //if you are chained to the ball
	bool isUp = false; // if the first platform is up
	bool isUp2 = false; //if the second platform is up
	bool doorSwitch = false;
	bool move = false; //if you are in motion
	bool carryB = false;
	bool complete = false; //if you've beaten the level
	bool onGround = true;


	//Rectangles
	Rect switchRect;
	Rect playerRect;
	Rect powerRect;
	Rect switchDoor;
	Rect ballRect;
	Rect exitRect;
	Rect crateRect;
	Rect jumpRect;
	Rect ledgeRect;

	std::vector<Sprite*> platVec;
	std::vector<Sprite*> objectVector;
	DrawNode* chainLine;

	Camera* followcamera;

	EventListenerPhysicsContact* PhysicsEvent;

	PhysicsMaterial groundMat;

	static PhysicsWorld* physicsWorld;

	static Scene* sceneHandle;

	Color4F chainColour;
};




#include "Menu.h"
#include "Select.h"
#include "Level1.h"
#include "Level2.h"
#include "Level3.h"
#include "Level4.h"
#include "Level5.h"
#include "SimpleAudioEngine.h"
#include "Inputs.h"
#include "Display.h"
USING_NS_CC;

Scene* Startup::createScene()
{
	// 'scene' is an autorelease object
	Scene* menu = Scene::createWithPhysics();
	Startup* layer = Startup::create();

	menu->addChild(layer);

	sceneHandle = menu;

	Vec2 winSize = Director::getInstance()->getWinSizeInPixels();
	//Get the physics world from the scene so that we can work with it later
	//If we didn't do this, we would have to call director->getRunningScene()->getPhysicsWorld() every time we wanted to do something to the physics world
	physicsWorld = menu->getPhysicsWorld();
	return menu;
}

void Startup::onExit()
{
	Scene::onExit();
}

void Startup::onEnter()
{
	Scene::onEnter();
}

static void problemLoading(const char* filename)
{

}


bool Startup::init()
{
	if (!Scene::init())
	{
		return false;
	}
	director = Director::getInstance();

	auto visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	DrawWorld();

	auto Play_Image = MenuItemImage::create("Images/Menu/New Game.png", "Images/Menu/New Game glow.png", CC_CALLBACK_1(Startup::PlayImage, this));
	auto Select_Image = MenuItemImage::create("Images/Menu/Level Select.png", "Images/Menu/Level Select glow.png", CC_CALLBACK_1(Startup::SelectImage, this));
	auto Exit_Image = MenuItemImage::create("Images/Menu/Exit.png", "Images/Menu/Exit glow.png", CC_CALLBACK_1(Startup::ExitImage, this));

	Play_Image->setPosition(Point(445, 650));
	Select_Image->setPosition(Point(525, 440));
	Exit_Image->setPosition(Point(242, 230));

	auto *menu = Menu::create(Play_Image, Select_Image, Exit_Image, NULL);
	menu->setPosition(0, 0);
	this->addChild(menu);


	this->scheduleUpdate();

	return true;
}

void Startup::PlayImage(cocos2d::Ref *pSender)
{
	CCLOG("PLAY Image");

	Scene *level5 = Level5::createScene();

	Director::getInstance()->replaceScene(TransitionFade::create(2.0, level5));
}

void Startup::SelectImage(cocos2d::Ref *pSender)
{
	CCLOG("SELECT Image");

	Scene *select = Selection::createScene();

	Director::getInstance()->pushScene(select);

}

void Startup::ExitImage(cocos2d::Ref *pSender)
{
	CCLOG("EXIT Image");
	exit(1);
}

void Startup::update(float deltaTime)
{

	updateInputs();

}

void Startup::updateInputs()
{
	updateKeyboardInputs();
	updateMouseInputs();
}

void Startup::updateMouseInputs()
{

}

void Startup::updateKeyboardInputs()
{
	if (INPUTS->getKeyPress(KeyCode::KEY_ENTER))
	{
		Scene *level5 = Level5::createScene();

		Director::getInstance()->replaceScene(TransitionFade::create(2.0, level5));
	}

	if (INPUTS->getKeyPress(KeyCode::KEY_ESCAPE))
	{
		exit(1);
	}

}


void Startup::DrawWorld()
{
	scene = Sprite::create("Images/Menu/menu.png");
	scene->setPosition(960, 540);
	this->addChild(scene);
}
//Init the static physics world pointer. Set it to be a nullptr which means it points to nothing
PhysicsWorld* Startup::physicsWorld = nullptr;
Scene* Startup::sceneHandle = nullptr;
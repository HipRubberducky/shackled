// By: Digital Takeout (2018)

#include "Level5.h"
#include "Level2.h"
#include "SimpleAudioEngine.h"
#include "Inputs.h"
#include "Display.h"
#include <iomanip>
#include <vector>
#include "Custom_Objects.h"

//using namespace CocosDenshion;

using namespace cocos2d;
USING_NS_CC;

Scene* Level5::createScene()
{
	// 'scene' is an autorelease object
	Scene* level5 = Scene::createWithPhysics();
	Level5* layer = Level5::create();

	//level5->getPhysicsWorld()->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_ALL);

	level5->addChild(layer);

	sceneHandle = level5;

	Vec2 winSize = Director::getInstance()->getWinSizeInPixels();
	//Get the physics world from the scene so that we can work with it later
	//If we didn't do this, we would have to call director->getRunningScene()->getPhysicsWorld() every time we wanted to do something to the physics world
	physicsWorld = level5->getPhysicsWorld();

	physicsWorld->setGravity(Vec2(0, -980));

	/////////FOR LEVEL LAYOUT/////////////
	//auto scale = ScaleBy::create(0.5, 2.9);
	//scene->runAction(scale);
	/////////////////////////////////////

	return level5;

}

void Level5::onExit()
{
	Scene::onExit();
}

void Level5::onEnter()
{
	//DISPLAY->createDebugConsole(true);
	std::cout << "Testing..." << std::endl;
	Scene::onEnter();

}


static void problemLoading(const char* filename)
{

}

//auto audio1 = SimpleAudioEngine::getInstance(); //initiates the audio engine

bool Level5::init()
{
	if (!Scene::init())
	{
		return false;
	}

	//AUDIO STUFF
	//audio1->playBackgroundMusic("Audio/Deponia Soundtrack - Junk.mp3", true);
	//audio1->preloadEffect("Audio/MetalSound.mp3"); //preloading writes it to a cache so you don't have to waste time in loading it in every time and get rid of the delay: DOESN'T WORK REALLY
	//audio1->preloadEffect("Audio/CantSound.mp3");


	//automatic stuff
	director = Director::getInstance();
	auto visibleSize = Director::getInstance()->getVisibleSize();
	Point origin = Director::getInstance()->getVisibleOrigin();

	DrawWorld();


	this->setCameraMask((unsigned short)CameraFlag::USER1, false); //determines what can and can't be seen


	getDefaultCamera()->setScaleX(0.7f); //getting the default camera and scaling it down so what you see is bigger
	getDefaultCamera()->setScaleY(0.7f); //X and Y 
	getDefaultCamera()->setPosition(Vec2(950 * getDefaultCamera()->getScaleX(), 370)); //sets it so that by default nothing is off screen, it will take the scale of the camera into effect
																					   //nothing will be off screen but it will be as far left as it can go


																					   //SPRITES
	ball = Sprite::create("Images/Character/ball.png"); //grabbing image
	ball->setScale(0.25f); //scale of it
	ball->setAnchorPoint(Vec2(0.5f, 0.5f)); //set anchor point to middle
	ball->setPosition(Vec2(280, 908)); //setting position
	PhysicsBody* ball_body = PhysicsBody::createBox(ball->getContentSize()); //adds a physics body to it, which is a box and the size of the png
	ball_body->setDynamic(true); //can you knock it around or not
	ball_body->setMass(2000);
	ball_body->setRotationEnable(false); //can it rotate or not
	ball->setPhysicsBody(ball_body); //assigning the physics body

	g_exit = Sprite::create("Images/Assets/signGlow.png");
	g_exit->setAnchorPoint(Vec2(0.5f, 0.5f));
	g_exit->setScale(0.037);
	g_exit->setFlippedX(true);
	g_exit->setPosition(Vec2(2080, 405));
	PhysicsBody* exit_body = PhysicsBody::createBox(g_exit->getContentSize());
	exit_body->setDynamic(false);
	exit_body->setRotationEnable(false);
	g_exit->setPhysicsBody(exit_body);

	power = Sprite::create("Images/Assets/boltMan.png");
	power->setScale(0.25f);
	power->setAnchorPoint(Vec2(0.5f, 0.5f));
	power->setPosition(Vec2(250, 200));
	PhysicsBody* power_body = PhysicsBody::createBox(power->getContentSize());
	power_body->setDynamic(false);
	power_body->setRotationEnable(false);
	power->setPhysicsBody(power_body);

	//MISTER SHACKLED HIMSELF
	SpriteFrameCache* cacher = SpriteFrameCache::getInstance(); //getting the sprite frame cache
	cacher->addSpriteFramesWithFile("Animation/char.plist"); //add a .plist file, tell the animation what to do
	SpriteFrame* spriteFrame = cacher->getSpriteFrameByName("stand.png"); //default frame
	character = Sprite::createWithSpriteFrameName("stand.png"); //actually make the character below like making the other objects in the scene
	character->setScale(0.25f);
	character->setAnchorPoint(Vec2(0.5f, 0.5f));
	character->setPosition(Vec2(200, 970));
	PhysicsBody* chara_body = PhysicsBody::createBox(character->getContentSize());
	chara_body->setDynamic(true);
	chara_body->setRotationEnable(false);
	character->setPhysicsBody(chara_body);
	character->getPhysicsBody()->setVelocityLimit(600); //his velocity limit 
	chara_body->setMass(80); //giving him mass
	moveSpeed = 35000; //move speed/how much force we apply when he wants to move

	jumpSprite = Sprite::create("Images/Assets/crate.png");
	jumpSprite->setScaleX(0.375);
	jumpSprite->setScaleY(0.083);
	//jumpSprite->setPhysicsBody(jumpBody);
	//character->addChild(jumpSprite);
	jumpSprite->setPosition(character->getPositionX(), character->getPositionY() - 55);
	this->addChild(jumpSprite);
	jumpSprite->setVisible(false);

	ledgeSprite = Sprite::create("Images/Assets/crate.png");
	ledgeSprite->setScale(0.167);
	ledgeSprite->setPosition(character->getPositionX() + (100 * character->getScaleX()), character->getPositionY() - 65);
	this->addChild(ledgeSprite);
	ledgeSprite->setVisible(false);

	//Rect jumpRect(character->getPositionX(), character->getPositionY() - 53, 10, 10);

	Animation* anim = Animation::create(); //make an animation
	Animation* bAnim = Animation::create();

	for (int i = 1; i < 32; i++) //assign 35 different individual pngs to the animation reel
	{
		std::ostringstream fileName; //accessing the files
		fileName << "char (" << i << ").png";
		SpriteFrame *frame = cacher->getSpriteFrameByName(fileName.str().c_str());
		anim->addSpriteFrame(frame);
	}

	//for (int x = 1; x < 35; x++)
	//{
	//	std::ostringstream ballName;
	//	ballName << "ball-(" << x << ").png";
	//	SpriteFrame *bFrame = cacher->getSpriteFrameByName(ballName.str().c_str());
	//	anim->addSpriteFrame(bFrame);
	//}

	anim->setDelayPerUnit(0.02f); //how long each frame is displayed

	character->runAction(RepeatForever::create(Animate::create(anim))); //hey run forever, but only when we tell you to
																		//character->runAction(RepeatForever::create(Animate::create(bAnim)));

	chainLine = DrawNode::create(); //drawing the line

	this->addChild(chainLine);

	auto sceneEdge = PhysicsBody::createEdgeBox(visibleSize * 2.85, PHYSICSBODY_MATERIAL_DEFAULT, 3); //boundaries of the level

	auto edgeNode = Node::create();
	edgeNode->setPosition(Point(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y)); //bounds of the camera 
	edgeNode->setPhysicsBody(sceneEdge);

	//putting everything in the scene
	this->addChild(character);
	this->addChild(edgeNode);
	this->addChild(ball);
	this->addChild(power);
	this->addChild(g_exit);

	objectVector.push_back(ball);

	this->scheduleUpdate();

	return true;
}

void Level5::Play(cocos2d::Ref *pSender)
{
	CCLOG("Play");
}

void Level5::update(float deltaTime)
{

	if (move == false) //if we're not telling the character to move, stop animation and reset to first frame
	{
		character->pauseSchedulerAndActions();
		character->setSpriteFrame("stand.png");
	}

	else //if we are moving just keep running
	{
		character->resumeSchedulerAndActions();
	}


	UpdatePositions(deltaTime); //position updating stuff
	UpdateRot(deltaTime);
	CheckCollision(); //checking collisions
	updateInputs(); //handling all of our inputs


	INPUTS->clearForNextFrame();


	if (!hasObjectChild) //checks if you are carrying something, if you are NOT then it calculates the vector between you and the ball
		chainVec = character->getPosition() - ball->getPosition(); //vector between you and ball
	else //if you ARE carrying something
		chainVec.set(0, 0); //set the vector to zero and delete line
	chainLine->clear(); //delete it every time so you only have one line on your screen at a time


	if (chained) { //if you are chained 
		if (chainVec.getLength() <= maxChainLength) { //and the vector is less than max chain length
			if (chainVec.getLength() < 150) //and less than 100 units
				chainColour = Color4F::BLUE;
			else //greater than 100 but within max chain length
				chainColour = Color4F::GREEN;
			if (hasPassedChainLength) { //only stop you if it hasn't tried to stop you before
				hasPassedChainLength = false;
			}
		}
		else { //greater than/equal to max chain length
			chainColour = Color4F::RED;
			if (!hasPassedChainLength) {
				character->getPhysicsBody()->setVelocity(Vec2(0, 0)); //stops you
				hasPassedChainLength = true; //so it won't try to stop you again
			}
		}
		if (chained && !hasObjectChild) //draw the chain if it's not broken and you aren't carrying an object 
			chainLine->drawLine(character->getPosition(), ball->getPosition(), chainColour);
	}


}

void Level5::updateInputs()
{
	updateKeyboardInputs();
	updateMouseInputs();
}

void Level5::updateMouseInputs()
{

}

void Level5::updateKeyboardInputs()
{

	//Jump SPACE
	if (INPUTS->getKeyPress(KeyCode::KEY_SPACE)) //|| XboxController->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_A)
	{
		if (!chained || chainVec.getLength() <= maxChainLength) //if you jump and are not chained or within max length you jump
		{
			if (jumpCheck()) {
			character->getPhysicsBody()->applyForce(Vec2(0, jumpHeight)); //"applying a force 1 time up to jump"
																		  }
		}
	}

	//Right Movement D KEY
	if (INPUTS->getKey(KeyCode::KEY_D)) //|| XboxController->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_LEFT)
	{
		if (!chained || chainVec.getLength() <= maxChainLength || character->getPositionX() < ball->getPositionX()) {
			character->setScaleX(-0.25f); //flip sprite
			if (walkCheck()) {
				//if you are not chained, or within max chain limit/at it/below it on the x position
				character->getPhysicsBody()->applyForce(Vec2(moveSpeed, 0)); //applying force to move
				move = true;
			}
			else if (jumpCheck())
				character->getPhysicsBody()->setVelocity(Vec2(0, 0));
		}
	}

	if (INPUTS->getKeyRelease(KeyCode::KEY_D)) //on release of D KEY
	{
		move = false;
	}

	//Left Movement A KEY
	if (INPUTS->getKey(KeyCode::KEY_A)) //|| XboxController->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_LEFT)
	{
		if (!chained || chainVec.getLength() <= maxChainLength || character->getPositionX() > ball->getPositionX()) {
			character->setScaleX(0.25f);
			if (walkCheck()) {
				character->getPhysicsBody()->applyForce(Vec2(-moveSpeed, 0));
				move = true;
			}
			else if (jumpCheck())
				character->getPhysicsBody()->setVelocity(Vec2(0, 0));
			
		}
	}

	if (INPUTS->getKeyRelease(KeyCode::KEY_A))
	{
		move = false;
	}

	//Picking up KEY L
	if (INPUTS->getKeyPress(KeyCode::KEY_L)) //|| XboxController->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_X)
	{
		if (!hasObjectChild) { //if you aren't carrying something
			//Vec2 tempVec;
			//tempVec = character->getPosition() - ball->getPosition(); //creating a vector between character and other objects in level (already have one for ball) now you pick up ball
			//if (chainVec.getLength() < tempVec.getLength() && chainVec.getLength() < 150) { //if the ball is closer to you than object and within 100 units of ball
				ball->retain(); //keeps "copy" of ball so you can reference it again
				ball->removeFromParent(); //remove it from parent(scene) 
				character->addChild(ball); //parent ball to character 
				ball->release(); //opposite of retain 
				ball->setScale(1.00f); //rescale it so it's not teeny tiny
				ball->setPosition(100, 570);
				ball->getPhysicsBody()->setMass(0);
				ball->getPhysicsBody()->setDynamic(false);
				hasObjectChild = true;
				moveSpeed = 35000; //reduce move speed 
			//}
			//else { //not within range of anything 
				   //audio1->playEffect("Audio/CantSound.mp3");
			//}
		}
		else if (ball->getParent() == character && walkCheck()) { //if you are carrying the ball now you are putting it down doing the opposite of above
			ball->retain();
			ball->removeFromParent();
			this->addChild(ball);
			ball->release();
			ball->setPosition(character->getPositionX() - 55 * character->getScaleX() * 4, character->getPositionY() - 30);
			ball->setScale(0.25f);
			ball->getPhysicsBody()->setMass(2000);
			ball->getPhysicsBody()->setDynamic(true);
			ball->getPhysicsBody()->setRotationEnable(false);
			hasObjectChild = false;
			moveSpeed = 45000;
			//audio1->playEffect("Audio/MetalSound.mp3");

		}
		else { //not putting down anything
			   //audio1->playEffect("Audio/CantSound.mp3");
		}
	}

	if (INPUTS->getKeyPress(KeyCode::KEY_M)) //to unchain yourself DEBUGGING
	{
		chained = false;
	}

	if (INPUTS->getKeyPress(KeyCode::KEY_Q)) //to exit the game 
	{
		exit(1);
	}

}

void Level5::UpdatePositions(float dt)
{
	//updating camera
	if (character->getPositionX() > 960 * getDefaultCamera()->getScaleX() && character->getPositionX() < 1480) //has camera follow the character horizontally, stop when it gets to edge of screen
	{
		getDefaultCamera()->setPositionX(character->getPositionX());
	}

	if (character->getPositionY() > 430 * getDefaultCamera()->getScaleY() && character->getPositionY() < 950) //has camera follow the character horizontally, stop when it gets to edge of screen
	{
		getDefaultCamera()->setPositionY(character->getPositionY());
	}

	jumpSprite->setPosition(character->getPositionX(), character->getPositionY() - 55);
	ledgeSprite->setPosition(character->getPositionX() + (-200 * character->getScaleX()), character->getPositionY() - 85);
}

void Level5::UpdateRot(float dt)
{

}

void Level5::CheckCollision()
{

	//PhysicsEvent

	//to see if the rectangles are touching 
	playerRect = character->getBoundingBox();
	powerRect = power->getBoundingBox();
	ballRect = ball->getBoundingBox();
	exitRect = g_exit->getBoundingBox();
	jumpRect = jumpSprite->getBoundingBox();
	ledgeRect = ledgeSprite->getBoundingBox();

	//Actions for the moving platforms - horrible inefficient 
	MoveTo* up = MoveTo::create(2.5, Point(232, 203));


	if (playerRect.intersectsRect(powerRect)) //if the player interacts with the power up the character is now unshackled 
	{
		chained = false;
		power->setPosition(Vec2(10000, 10000));
	};

	//Win Condition
	if (exitRect.intersectsRect(playerRect)) //if player touches exit door, game exits 
	{
		complete = true;
		//audio1->stopBackgroundMusic();
		Scene *level2 = Level2::createScene();

		Director::getInstance()->replaceScene(TransitionFade::create(2, level2));
		
	};

	//for (int i = 0; i < platVec.size(); i++) 
	//{
	//	for (int i = 0; i < platVec.size(); i++) {
	//		Rect tempRect = platVec[i]->getBoundingBox();
	//		std::cout << "TEMP RECT: " << tempRect.getMidX() << std::endl;
	//		std::cout << "JUMP RECT: " << jumpRect.getMidX() << std::endl;
	//		if (tempRect.intersectsRect(jumpRect)) {
	//			std::cout << "IT'S TRUE!" << std::endl;
	//			canJump = true;
	//		}
	//		else
	//			canJump = false;
	//	}
	//}

}



void Level5::DrawWorld()
{

	makePlat(2, 28, -1, 2);
	makePlat(38, 3, -1, -1);
	makePlat(9, 3, 28, 2);
	makePlat(6, 4, 1, 8);
	makePlat(1, 1, 7, 10);
	makePlat(2, 1, 7, 9);
	makePlat(3, 1, 7, 8);
	makePlat(5, 1, 6, 7);
	makePlat(5, 1, 7, 6);
	makePlat(5, 1, 8, 5);

	makeBack(40, 40, -1, -1);

	/*Size tempSize(100, 100);
	Sprite* tempBox = Sprite::create("Images/Assets/newMetalTile.png");
	tempBox->setScale(0.1, 0.1);
	tempBox->setPhysicsBody(PhysicsBody::createBox(tempSize));
	tempBox->setPosition(200, 200);*/

}
//Init the static physics world pointer. Set it to be a nullptr which means it points to nothing
PhysicsWorld* Level5::physicsWorld = nullptr;
Scene* Level5::sceneHandle = nullptr;

void Level5::makePlat(int width, int height, int xPos, int yPos)
{
	xPos *= 58; yPos *= 58;
	for (int j = 0; j < height; j++) {
		for (int i = 0; i < width; i++) {
			Sprite* tempSprite = Sprite::create("Images/Assets/newMetalTile.png");
			tempSprite->setAnchorPoint(Vec2(0, 0));
			tempSprite->setScale(0.125);
			tempSprite->setPosition(xPos + 58 * i, yPos + 58 * j);
			platVec.push_back(tempSprite);
			this->addChild(tempSprite);

		}
	}

	Size tempSize(width * 58, height * 58);
	Sprite* tempBox = Sprite::create();
	PhysicsBody* tempBody = PhysicsBody::createBox(tempSize);
	tempBody->setDynamic(false);
	tempBody->setRotationEnable(false);
	tempBox->setPhysicsBody(tempBody);
	tempBox->setAnchorPoint(Vec2(0, 0));
	tempBox->setPosition(xPos + width * 58 / 2, yPos + height * 58 / 2);
	this->addChild(tempBox);
}

void Level5::makeBack(int width, int height, int xPos, int yPos)
{
	xPos *= 162; yPos *= 162;
	for (int j = 0; j < height; j++) {
		for (int i = 0; i < width; i++) {
			Sprite* tempSprite = Sprite::create("Images/Assets/bricks.jpg");
			tempSprite->setAnchorPoint(Vec2(0, 0));
			tempSprite->setScale(0.35);
			tempSprite->setPosition(xPos + 162 * i, yPos + 162 * j);
			this->addChild(tempSprite, -100);

		}
	}
}

bool Level5::jumpCheck()
{
	jumpRect = jumpSprite->getBoundingBox();
	for (int i = 0; i < platVec.size(); i++) {
		Rect tempRect = platVec[i]->getBoundingBox();

		if (jumpRect.intersectsRect(tempRect))
		{
			onGround = true;
			return true;
		}
	}
	onGround = false;
	for (int j = 0; j < objectVector.size(); j++) {
		Rect tempRect = objectVector[j]->getBoundingBox();
		if (jumpRect.intersectsRect(tempRect))
		{
			return true;
		}
	}
	return false;
}

bool Level5::walkCheck() {
	if (!chained || hasObjectChild)
		return true;
	if (jumpCheck()) {
		if (!onGround)
			return true;
		for (int i = 0; i < platVec.size(); i++) {
			Rect tempRect = platVec[i]->getBoundingBox();

			if (ledgeRect.intersectsRect(tempRect)) {
				return true;
			}
		}
	}
	for (int j = 0; j < objectVector.size(); j++) {
		Rect tempRect = objectVector[j]->getBoundingBox();
		if (ledgeRect.intersectsRect(tempRect))
		{
			return true;
		}
	}
	return false;
}
#pragma once
#include "cocos2d.h"
using namespace cocos2d;
class Platform :public Sprite {
private:
	float x;
	float y;
	float width;
	float height;
	PhysicsBody* body;
public:
	Platform() {};
	Platform(float w, float h, float _x, float _y) {
		Size tempSize(w, h);
		body = PhysicsBody::createBox(tempSize);
		setPosition(_x, _y);
		x = _x; y = _y; width = w; height = h;
	};
	static Sprite* makePlat() {
		Sprite temp;
		Size tempSize(200, 20);
		temp.setPhysicsBody(PhysicsBody::createBox(tempSize));
		temp.setPosition(300, 270);
		return &temp;
	};
};

class MovingPlatform :public Sprite {
private:
	float x;
	float y;
	float width;
	float height;
	float distance;
	float duration;
	bool startsDown;
	PhysicsBody* body;
	MoveBy* platMove;
public:
	MovingPlatform() {};
	void setSize(float w, float h) {
		width = w;
		height = h;
		Size tempSize(w, h);
		body = PhysicsBody::createBox(tempSize);
	};
	void setPosition(float _x, float _y) {
		x = _x;
		y = _y;
		setPosition(x, y);
	}
	void setDistance(float d) {
		distance = d;
	}
	void setDuration(float d) {
		duration = d;
	}
	bool downStart(bool s) {
		startsDown = s;
		return startsDown;
	}
	MoveBy* makeAction() {
		if (startsDown) {
			platMove = MoveBy::create(duration, Vec2(x, y + distance));
		}
		else {
			platMove = MoveBy::create(duration, Vec2(x, y - distance));
		}
		return platMove;
	}
};
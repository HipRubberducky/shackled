#pragma once

#include "cocos2d.h"
#include <iostream>
#include <fstream>
#include <ui\UITextField.h>
using namespace cocos2d;
class Selection : public cocos2d::Scene
{
public:
	CREATE_FUNC(Selection);
	static cocos2d::Scene* createScene();
	virtual void onExit();
	virtual void onEnter();
	virtual bool init();
	void update(float deltaTime);
	void updateInputs();
	void updateMouseInputs();
	void updateKeyboardInputs();

	void menuCloseCallback(cocos2d::Ref* pSender);
	void DrawWorld();

	void Level1(Ref *pSender);
	void Level2(Ref *pSender);
	void Level3(Ref *pSender);
	void Level4(Ref *pSender);
	void Level5(Ref *pSender);
	void Back(Ref *pSender);

	float radius;
	float angle;
	float rotfun;

private:

	Director* director;

	Sprite* scene;
	Sprite* title;

	static PhysicsWorld* physicsWorld;

	static Scene* sceneHandle;
};





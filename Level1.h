#pragma once

#include "cocos2d.h"
#include <iostream>
#include <fstream>
#include <ui\UITextField.h>

using namespace cocos2d;
class HelloWorld : public cocos2d::Scene
{
public:
	CREATE_FUNC(HelloWorld);
	static cocos2d::Scene* createScene();
	virtual void onExit();
	virtual void onEnter();
	virtual bool init();
	void update(float deltaTime);
	void updateInputs();
	void updateMouseInputs();
	void updateKeyboardInputs();
	void UpdatePositions(float dt);
	void UpdateRot(float dt);
	void CheckCollision();
	void makePlat(int width, int height, int xPos, int yPos);
	void makeBack(int width, int height, int xPos, int yPos);
	bool jumpCheck();
	bool walkCheck();

	void menuCloseCallback(cocos2d::Ref* pSender);
	void DrawWorld();

	void Play(Ref *pSender);

	Vec2 chainVec;
	float maxChainLength = 350;

	bool hasPassedChainLength = false;
	bool hasObjectChild = false;

	float moveSpeed;
	float jumpHeight = 2850000;

private:

	Director* director;

	//Character
	Sprite* character;
	Sprite* ball;

	//Objects
	Sprite* power;
	Sprite* buttonBase1;
	Sprite* button1;
	Sprite* buttonDoorBase;
	Sprite* buttonDoor;
	Sprite* buttonBase2;
	Sprite* button2;
	Sprite* door;
	Sprite* g_exit;
	Sprite* movPlat;
	Sprite* movPlat2;
	Sprite* ironDoor;
	Sprite* crate;
	Sprite* jumpSprite;
	Sprite* ledgeSprite;

	//Background
	Sprite* background;
	Sprite* background2;
	Sprite* platform;
	Sprite* platform1;
	Sprite* platform2;
	Sprite* platform3;
	Sprite* platform4;
	Sprite* platform5;
	Sprite* platform6;
	Sprite* platform7;
	Sprite* platform8;
	Sprite* platform9;
	Sprite* platform10;
	Sprite* platform11;
	Sprite* platPath;
	Sprite* platPath2;
	Sprite* platformTemp;

	bool chained = true; //if you are chained to the ball
	bool isUp = false; // if the first platform is up
	bool isUp2 = false; //if the second platform is up
	bool move = false; //if you are in motion
	bool complete = false; //if you've beaten the level
	bool onGround = true;


	//Rectangles
	Rect switchRect;
	Rect playerRect;
	Rect powerRect;
	Rect switchDoor;
	Rect switchRect2;
	Rect ballRect;
	Rect exitRect;
	Rect crateRect;
	Rect jumpRect;
	Rect ledgeRect;

	std::vector<Sprite*> platVec;
	std::vector<Sprite*> objectVector;
	DrawNode* chainLine;

	Camera* followcamera;

	EventListenerPhysicsContact* PhysicsEvent;

	PhysicsMaterial groundMat;

	static PhysicsWorld* physicsWorld;

	static Scene* sceneHandle;

	Color4F chainColour;
};

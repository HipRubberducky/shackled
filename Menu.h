#pragma once

#include "cocos2d.h"
#include <iostream>
#include <fstream>
#include <ui\UITextField.h>
using namespace cocos2d;
class Startup : public cocos2d::Scene
{
public:
	CREATE_FUNC(Startup);
	static cocos2d::Scene* createScene();
	virtual void onExit();
	virtual void onEnter();
	virtual bool init();
	void update(float deltaTime);
	void updateInputs();
	void updateMouseInputs();
	void updateKeyboardInputs();
	void UpdatePositions(float dt);
	void UpdateRot(float dt);
	void CheckCollision();

	void menuCloseCallback(cocos2d::Ref* pSender);
	void DrawWorld();

	void PlayImage(Ref *pSender);
	void SelectImage(Ref *pSender);
	void ExitImage(Ref *pSender);

	float radius;
	float angle;
	float rotfun;

private:

	Director* director;

	Sprite* scene;
	Sprite* title;

	static PhysicsWorld* physicsWorld;

	static Scene* sceneHandle;
};






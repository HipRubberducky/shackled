#include "Menu.h"
#include "Select.h"
#include "Level1.h"
#include "Level2.h"
#include "Level3.h"
#include "Level4.h"
#include "Level5.h"
#include "SimpleAudioEngine.h"
#include "Inputs.h"
#include "Display.h"
USING_NS_CC;

Scene* Selection::createScene()
{
	// 'scene' is an autorelease object
	Scene* select = Scene::createWithPhysics();
	Selection* layer = Selection::create();

	select->addChild(layer);

	sceneHandle = select;

	Vec2 winSize = Director::getInstance()->getWinSizeInPixels();
	//Get the physics world from the scene so that we can work with it later
	//If we didn't do this, we would have to call director->getRunningScene()->getPhysicsWorld() every time we wanted to do something to the physics world
	physicsWorld = select->getPhysicsWorld();
	return select;
}

void Selection::onExit()
{
	Scene::onExit();
}

void Selection::onEnter()
{
	Scene::onEnter();
}

static void problemLoading(const char* filename)
{

}


bool Selection::init()
{
	if (!Scene::init())
	{
		return false;
	}
	director = Director::getInstance();

	auto visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	DrawWorld();

	//auto Play_Image = MenuItemImage::create("Images/Menu/New Game.png", "Images/Menu/New Game glow.png", CC_CALLBACK_1(Startup::PlayImage, this));
	//auto Select_Image = MenuItemImage::create("Images/Menu/Level Select.png", "Images/Menu/Level Select glow.png", CC_CALLBACK_1(Startup::SelectImage, this));
	//auto Exit_Image = MenuItemImage::create("Images/Menu/Exit.png", "Images/Menu/Exit glow.png", CC_CALLBACK_1(Startup::ExitImage, this));
	//
	//Play_Image->setPosition(Point(445, 650));
	//Select_Image->setPosition(Point(525, 440));
	//Exit_Image->setPosition(Point(242, 230));
	//
	//auto *menu = Menu::create(Play_Image, Select_Image, Exit_Image, NULL);
	//menu->setPosition(0, 0);
	//this->addChild(menu);

	auto Level1 = MenuItemImage::create("Images/Menu/Level1.png", "Images/Menu/Level1 glow.png", CC_CALLBACK_1(Selection::Level1, this));
	auto Level2 = MenuItemImage::create("Images/Menu/Level2.png", "Images/Menu/Level2 glow.png", CC_CALLBACK_1(Selection::Level2, this));
	auto Level3 = MenuItemImage::create("Images/Menu/Level3.png", "Images/Menu/Level3 glow.png", CC_CALLBACK_1(Selection::Level3, this));
	auto Level4 = MenuItemImage::create("Images/Menu/Level4.png", "Images/Menu/Level4 glow.png", CC_CALLBACK_1(Selection::Level4, this));
	//auto Level5 = MenuItemImage::create("Images/Menu/Level5.png", "Images/Menu/Level5 glow.png", CC_CALLBACK_1(Selection::Level5, this));

	Level1->setPosition(Point(310, 650));
	Level2->setPosition(Point(310, 525));
	Level3->setPosition(Point(310, 400));
	Level4->setPosition(Point(310, 275));
	//Level5->setPosition(Point(310, 150));

	auto *selected = Menu::create(Level1, Level2, Level3, Level4, NULL);
	selected->setPosition(0, 0);
	this->addChild(selected);

	this->scheduleUpdate();

	return true;
}

void Selection::Level1(cocos2d::Ref *pSender)
{
	CCLOG("LEVEL1");

	Scene *level5 = Level5::createScene();

	Director::getInstance()->replaceScene(TransitionFade::create(2.0, level5));
}

void Selection::Level2(cocos2d::Ref *pSender)
{
	CCLOG("LEVEL2");

	Scene *level2 = Level2::createScene();

	Director::getInstance()->replaceScene(TransitionFade::create(2.0, level2));
}

void Selection::Level3(cocos2d::Ref *pSender)
{
	CCLOG("LEVEL3");

	Scene *level4 = Level4::createScene();

	Director::getInstance()->replaceScene(TransitionFade::create(2.0, level4));
}

void Selection::Level4(cocos2d::Ref *pSender)
{
	CCLOG("LEVEL4");

	//Scene *scene = HelloWorld::createScene();
	Scene *level3 = Level3::createScene();

	Director::getInstance()->replaceScene(TransitionFade::create(2.0, level3));
}

/*void Selection::Level5(cocos2d::Ref *pSender)
{
	CCLOG("LEVEL5");

	Scene *level3 = Level3::createScene();

	Director::getInstance()->replaceScene(TransitionFade::create(2.0, level3));
}*/

void Selection::update(float deltaTime)
{

	updateInputs();

}

void Selection::updateInputs()
{
	updateKeyboardInputs();
	updateMouseInputs();
}

void Selection::updateMouseInputs()
{

}

void Selection::updateKeyboardInputs()
{

	if (INPUTS->getKeyPress(KeyCode::KEY_ESCAPE))
	{
		exit(1);
	}

}


void Selection::DrawWorld()
{
	scene = Sprite::create("Images/Menu/menu.png");
	scene->setPosition(960, 540);
	this->addChild(scene);
}
//Init the static physics world pointer. Set it to be a nullptr which means it points to nothing
PhysicsWorld* Selection::physicsWorld = nullptr;
Scene* Selection::sceneHandle = nullptr;